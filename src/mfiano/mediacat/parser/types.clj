(ns mfiano.mediacat.parser.types
  (:refer-clojure :exclude [read read-string])
  (:require [mfiano.mediacat.parser.transformers :as tr]
            [mfiano.mediacat.parser.util :as p.util])
  (:import (com.tomgibara.bits Bits BitReader BitStore)
           (java.nio ByteBuffer)))

(def ^:private default-options
  {:endian :be
   :encoding :ascii
   :delimiter ""})

(defn- read-boolean
  [^BitReader bits]
  (.readBoolean bits))

(defn- read-bits
  [^BitReader bits {:keys [size]}]
  (if (<= size 32)
    (.readLong bits size)
    (.readBigInt bits size)))

(defn- read-bytes
  [^BitReader bits {:keys [size endian]}]
  (let [store (Bits/store (* size 8))]
    (.readFrom store bits)
    (let [bytes (.toByteArray store)]
      (case endian
        :be bytes
        :le (doto bytes tr/bytes-reverse!)))))

(defn- read-string
  [^BitReader bits {:keys [delimiter size] :as options}]
  (assert (or (seq delimiter) size)
          "String fields require either a delimiter or size.")
  (let [loc (.getPosition bits)
        bytes (p.util/bytes-seq bits options)
        delimited-bytes (p.util/get-delimited-string-bytes bytes options)
        ret (String. ^bytes delimited-bytes
                     ^String (p.util/convert-string-encoding options))]
    (when (seq delimiter)
      (.setPosition bits (+ loc
                            (* (count delimited-bytes) 8)
                            (* (count (p.util/get-string-bytes delimiter options)) 8))))
    ret))

(defn- read-float
  [^BitReader bits options]
  (let [bytes (read-bytes bits (assoc options :size 4))
        buffer (ByteBuffer/wrap bytes)]
    (.getFloat buffer)))

(defn- read-double
  [^BitReader bits options]
  (let [bytes (read-bytes bits (assoc options :size 8))
        buffer (ByteBuffer/wrap bytes)]
    (.getDouble buffer)))

(defn read
  [data-type bits & {:as options}]
  (let [options (merge default-options options)]
    (case data-type
      :boolean (read-boolean bits)
      :bits (read-bits bits options)
      :bytes (read-bytes bits options)
      :int (tr/bytes->int (read-bytes bits options))
      :uint (tr/bytes->uint (read-bytes bits options))
      :string (read-string bits options)
      :float (read-float bits options)
      :double (read-double bits options))))
