(ns mfiano.mediacat.parser.dispatch
  (:require [clojure.string :as str]
            [me.raynes.fs :as fs]))

(defmulti parse-file
  (fn [path]
    (-> path
        fs/extension
        (subs 1)
        str/lower-case
        keyword)))
