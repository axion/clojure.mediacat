(ns mfiano.mediacat.parser.util
  (:require [clojure.java.io :as io]
            [mfiano.mediacat.util :as util]
            [net.cgrand.xforms :as xf])
  (:import (com.tomgibara.bits Bits BitReader BitStreamException EndOfBitStreamException)
           (java.io InputStream RandomAccessFile)))

(defn make-lazy-bit-reader
  [xs]
  (let [current-byte (volatile! 0)
        current-bit (volatile! 0)
        current-rest (volatile! xs)]
    (reify BitReader
      (readBit [_]
        (when (empty? @current-rest)
          (throw (BitStreamException. "EOF while reading a bit.")))
        (let [ret (bit-and (unsigned-bit-shift-right
                            ^byte (first @current-rest)
                            (- 8 (inc ^int @current-bit)))
                           0x1)]
          (vswap! current-bit inc)
          (when (>= @current-bit 8)
            (vswap! current-rest rest)
            (vswap! current-byte inc)
            (vreset! current-bit 0))
          ret))
      (getPosition [_]
        (+ (* @current-byte 8) @current-bit))
      (setPosition [_ new-pos]
        (loop [xs xs]
          (when (empty? xs)
            (throw (BitStreamException. "EOF while setting position.")))
          (if (= (+ (* @current-byte 8) @current-bit) new-pos)
            (do (vreset! current-rest xs)
                new-pos)
            (do (vswap! current-bit inc)
                (when (>= @current-bit 8)
                  (vswap! current-byte inc)
                  (vreset! current-bit 0))
                (recur (rest xs)))))))))

(defn make-eof-bit-reader
  [path length & {offset :offset :or {offset 0}}]
  (let [stream (RandomAccessFile. path "r")
        bytes (byte-array offset)]
    (.seek stream (- length offset))
    (.readFully stream bytes)
    (Bits/readerFrom bytes)))

(defn convert-string-encoding
  [{:keys [encoding endian]}]
  (case encoding
    :ascii "US-ASCII"
    :latin1 "ISO-8859-1"
    :utf8 "UTF-8"
    :utf16 (if (= endian :be)
             "UTF-16BE"
             "UTF-16LE")
    (throw (UnsupportedOperationException. (format "Invalid string encoding: %s" encoding)))))

(defn get-string-bytes
  [s options]
  (vec (.getBytes ^String s ^String (convert-string-encoding options))))

(defn get-delimited-string-bytes
  [bytes {:keys [delimiter] :as options}]
  (let [delimiter-bytes (get-string-bytes delimiter options)
        xf (comp (xf/partition (count delimiter-bytes) 1)
                 (map-indexed vector)
                 (filter #(= (second %) delimiter-bytes))
                 (map first))
        delimiter-index (first (util/sequence* xf bytes))
        new-bytes (take (or delimiter-index 0) bytes)]
    (if (nil? delimiter-index)
      (byte-array bytes)
      (byte-array new-bytes))))

(defn bytes-seq
  [^BitReader bits {:keys [size]}]
  (letfn [(f [remaining]
            (when (or (nil? remaining)
                      (pos? remaining))
              (lazy-seq (try (cons (unchecked-byte (.read bits 8))
                                   (f (and remaining (dec remaining))))
                             (catch EndOfBitStreamException e
                               nil)))))]
    (f size)))

(defn calculate-duration
  [samples rate]
  (-> (/ samples rate)
      (util/round 2)))

(defn calculate-bit-rate
  [file-size duration]
  (-> (* file-size 8)
      (/ duration 1000)
      (util/round 2)))
