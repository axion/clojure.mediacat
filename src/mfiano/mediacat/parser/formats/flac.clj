(ns mfiano.mediacat.parser.formats.flac
  (:require [flatland.useful.seq :as seq]
            [mfiano.mediacat.parser.dispatch :as parser]
            [mfiano.mediacat.parser.formats.vorbis :as vorbis]
            [mfiano.mediacat.parser.io :as io]
            [mfiano.mediacat.parser.transformers :as tr]
            [mfiano.mediacat.parser.types :as types]
            [mfiano.mediacat.parser.util :as p.util]
            [mfiano.mediacat.util :as util]))

(defn- decode-metadata-type
  [type]
  (case type
    0 :stream-info
    4 :vorbis-comment
    :unused))

(defmulti ^:private parse-block-type
  (fn [file header]
    (:header/type header)))

(defn- parse-block
  [{bits :bits :as file}]
  (letfn [(parse-header []
            (hash-map :header/last? (types/read :boolean bits)
                      :header/type (decode-metadata-type (types/read :bits bits :size 7))
                      :header/length (types/read :uint bits :size 3)))]
    (let [header (parse-header)]
      (merge header (parse-block-type file header)))))

(defmethod parse-block-type :default
  [file header]
  (io/skip-bytes file (:header/length header))
  {})

(defmethod parse-block-type :stream-info
  [{:keys [bits length] :as file} _]
  (let [_ (types/read :bytes bits :size 10)
        sample-rate (types/read :bits bits :size 20)
        channels (inc (types/read :bits bits :size 3))
        bit-depth (inc (types/read :bits bits :size 5))
        samples (types/read :bits bits :size 36)
        _ (types/read :bytes bits :size 16)
        duration (p.util/calculate-duration samples sample-rate)
        bit-rate (p.util/calculate-bit-rate length duration)]
    {:audio/sample-rate sample-rate
     :audio/channels channels
     :audio-lossless/bit-depth bit-depth
     :audio-video/duration duration
     :audio-video/bit-rate-mode :variable
     :audio-video/bit-rate bit-rate}))

(defmethod parse-block-type :vorbis-comment
  [file _]
  (vorbis/parse-vorbis-comments file :ogg? false))

(defn- parse-marker
  [{:keys [path bits]}]
  (let [marker (types/read :string bits :size 4)]
    (assert (= marker "fLaC")
            (str "File is not a valid FLAC file: " path))))

(defn- parse-blocks
  [file]
  (let [blocks (repeatedly #(parse-block file))]
    (->> blocks
         (seq/take-until :header/last?)
         (into {})
         (util/remove-keys-by-ns "header"))))

(defmethod parser/parse-file :flac
  [path]
  (io/with-file file path nil
    (parse-marker file)
    (assoc (parse-blocks file)
           :common/format :flac
           :common/file-size (util/calculate-file-size file)
           :common/compression-mode :lossless)))
