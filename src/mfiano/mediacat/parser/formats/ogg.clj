(ns mfiano.mediacat.parser.formats.ogg
  (:require [mfiano.mediacat.parser.types :as types])
  (:import (java.io RandomAccessFile)))

(defn parse-page
  [{:keys [path bits] :as file}]
  (let [marker (types/read :string bits :size 4)
        version (types/read :uint bits :size 1)]
    (assert (and (= marker "OggS")
                 (= version 0))
            (str "Problem syncing to a page while reading Ogg file: " path))
    (let [_ (types/read :bits bits :size 5)
          last-page? (types/read :boolean bits)
          _ (types/read :bits bits :size 2)
          granule-pos (types/read :uint bits :size 8 :endian :le)
          _ (types/read :bytes bits :size 12)
          segment-count (types/read :uint bits :size 1)
          segment-lens (repeatedly segment-count #(types/read :uint bits :size 1))
          data-size (reduce + 0 segment-lens)]
      {:last-page? last-page?
       :granule-pos granule-pos
       :data-size data-size})))

(defn lazy-format-data
  [{bits :bits :as file}]
  (letfn [(f []
            (let [{:keys [data-size last-page?]} (parse-page file)
                  xs (types/read :bytes bits :size data-size)]
              (lazy-cat (seq xs)
                        (when-not last-page?
                          (f)))))]
    (f)))

(defn get-last-page-position
  [{:keys [path length]}]
  (with-open [stream (RandomAccessFile. path "r")]
    (let [bytes (byte-array 4)]
      (loop [offset (- length 4)]
        (.seek stream offset)
        (.readFully stream bytes)
        (if (= (String. bytes "US-ASCII") "OggS")
          (- (.getFilePointer stream) 4)
          (recur (dec offset)))))))
