(ns mfiano.mediacat.parser.formats.musepack
  (:require [mfiano.mediacat.parser.dispatch :as parser]
            [mfiano.mediacat.parser.formats.apev2 :as apev2]
            [mfiano.mediacat.parser.formats.musepack.sv7 :as sv7]
            [mfiano.mediacat.parser.formats.musepack.sv8 :as sv8]
            [mfiano.mediacat.parser.io :as io]
            [mfiano.mediacat.parser.types :as types]
            [mfiano.mediacat.util :as util]))

(defn- parse-marker
  [{:keys [path bits] :as file}]
  (let [marker (types/read :string bits :size 4)]
    (cond
      (= (subs marker 0 3) "MP+") (sv7/parse-sv7 file)
      (= marker "MPCK") (sv8/parse-sv8 file)
      :else (throw (Exception. (str "File is not a valid Musepack file: " path))))))

(defmethod parser/parse-file :mpc
  [path]
  (io/with-file file path nil
    (merge (apev2/parse-apev2 file)
           (parse-marker file)
           {:common/format :musepack
            :common/file-size (util/calculate-file-size file)
            :common/compression-mode :lossy
            :common/bit-rate-mode :variable})))
