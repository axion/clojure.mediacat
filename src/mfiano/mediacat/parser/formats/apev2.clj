(ns mfiano.mediacat.parser.formats.apev2
  (:require [mfiano.mediacat.parser.io :as io]
            [mfiano.mediacat.parser.types :as types]))

(defn- parse-header
  [path & {eof-offset :eof-offset}]
  (io/with-file {bits :bits} path eof-offset
    (when (= (types/read :string bits :size 8) "APETAGEX")
      (let [_ (types/read :uint bits :size 4 :endian :le)
            size (types/read :uint bits :size 4 :endian :le)
            count (types/read :uint bits :size 4 :endian :le)
            _ (types/read :bytes bits :size 4)]
        {:size size
         :count count}))))

(defn- parse-tags
  [bits count]
  (letfn [(parse-tag []
            (let [value-len (types/read :uint bits :size 4 :endian :le)
                  _ (types/read :bits bits :size 29)
                  type (types/read :bits bits :size 2)
                  _ (types/read :bits bits :size 1)
                  key (types/read :string bits :delimiter "\0")
                  value (types/read :string bits :size value-len)]
              (when (#{0 2} type)
                [key value])))]
    {:common/tags (->> (repeatedly count parse-tag)
                       (group-by first)
                       (reduce-kv (fn [m k v]
                                    (assoc m k (mapv second v)))
                                  {}))}))

(defn parse-apev2
  [{:keys [bits path] :as file}]
  (let [bof (parse-header path)
        eof (parse-header path :eof-offset 32)]
    (cond
      bof (parse-tags bits (:count bof))
      eof (io/with-file {bits :bits} path (:size eof)
            (parse-tags bits (:count eof))))))
