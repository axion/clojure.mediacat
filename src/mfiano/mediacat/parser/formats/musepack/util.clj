(ns mfiano.mediacat.parser.formats.musepack.util)

(defn decode-profile
  [n]
  (case n
    0 "No profile"
    1 "Unstable/Experimental"
    5 "below Telephone"
    6 "below Telephone"
    7 "Telephone"
    8 "Thumb"
    9 "Radio"
    10 "Standard"
    11 "Xtreme"
    12 "Insane"
    13 "BrainDead"
    14 "above BrainDead"
    15 "above BrainDead"))

(defn decode-sample-rate
  [n]
  (case n
    0 44100
    1 48000
    2 37800
    3 32000))
