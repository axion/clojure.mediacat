(ns mfiano.mediacat.parser.formats.musepack.sv7
  (:require [mfiano.mediacat.parser.formats.musepack.util :as mpc.util]
            [mfiano.mediacat.parser.types :as types]
            [mfiano.mediacat.parser.util :as p.util]
            [mfiano.mediacat.util :as util]))

(defn- lazy-format-data
  [bits]
  (let [xf (comp (partition-all 4)
                 (map reverse)
                 cat)]
    (util/sequence* xf (p.util/bytes-seq bits nil))))

(defn- decode-gain
  [n]
  (util/round (/ n 100.0) 2))

(defn- decode-peak
  [n]
  (util/round (/ n 65535.0) 2))

(defn parse-sv7
  [{:keys [length bits] :as file}]
  (let [bits (-> bits lazy-format-data p.util/make-lazy-bit-reader)
        frame-count (types/read :uint bits :size 4)
        samples (* frame-count 1152.0)
        max-level (types/read :bytes bits :size 1)
        profile (mpc.util/decode-profile (types/read :bits bits :size 4))
        _ (types/read :bits bits :size 2)
        sample-rate (mpc.util/decode-sample-rate (types/read :bits bits :size 2))
        _ (types/read :bytes bits :size 2)
        track-gain (decode-gain (types/read :int bits :size 2))
        track-peak (decode-peak (types/read :uint bits :size 2))
        album-gain (decode-gain (types/read :int bits :size 2))
        album-peak (decode-peak (types/read :uint bits :size 2))
        _ (types/read :boolean bits)
        duration (p.util/calculate-duration samples sample-rate)
        bit-rate (p.util/calculate-bit-rate length duration)]
    {:audio/sample-rate sample-rate
     :audio/channels 2
     :audio-video/duration duration
     :audio-video/bit-rate bit-rate
     :audio-replaygain/track-gain track-gain
     :audio-replaygain/track-peak track-peak
     :audio-replaygain/album-gain album-gain
     :audio-replaygain/album-peak album-peak
     :musepack/version :sv7
     :musepack/profile profile}))
