(ns mfiano.mediacat.parser.formats.musepack.sv8
  (:require [flatland.useful.seq :as seq]
            [mfiano.mediacat.parser.formats.musepack.util :as mpc.util]
            [mfiano.mediacat.parser.io :as io]
            [mfiano.mediacat.parser.types :as types]
            [mfiano.mediacat.util :as util]
            [mfiano.mediacat.parser.util :as p.util]))

(defn- decode-block-type
  [key]
  (case key
    "SH" :stream-header
    "RG" :replay-gain
    "EI" :encoder-info
    "SO" :seek-table-offset
    "AP" :audio-packet
    "ST" :seek-table
    "CT" :chapter-tag
    "SE" :stream-end))

(defn- decode-gain
  [n]
  (if (pos? n)
    (util/round (- 64.82 (/ n 256.0)) 2)
    0.0))

(defn- decode-peak
  [n]
  (util/round (/ (Math/pow 10 (/ n 5120.0)) 65535.0) 2))

(defn- parse-int
  [bits]
  (loop [count 1
         value 0]
    (let [octet (types/read :bits bits :size 8)
          value (bit-or (bit-shift-left value 7)
                        (bit-and octet 0x7f))]
      (if (bit-test octet 7)
        (recur (inc count) value)
        {:value value
         :byte-count count}))))

(defmulti ^:private parse-block-type
  (fn [file header]
    (:header/type header)))

(defn- parse-block
  [{bits :bits :as file}]
  (letfn [(parse-header []
            (let [key (types/read :string bits :size 2)
                  {:keys [value byte-count]} (parse-int bits)]
              (hash-map :header/type (decode-block-type key)
                        :header/size (- value byte-count 2))))]
    (let [header (parse-header)]
      (merge header (parse-block-type file header)))))

(defmethod parse-block-type :stream-header
  [{:keys [bits length] :as file} _]
  (let [_ (types/read :bytes bits :size 5)
        {samples :value} (parse-int bits)
        _ (parse-int bits)
        sample-rate (mpc.util/decode-sample-rate (types/read :bits bits :size 3))
        _ (types/read :bits bits :size 5)
        channels (inc (types/read :bits bits :size 4))
        _ (types/read :bits bits :size 4)
        duration (p.util/calculate-duration samples sample-rate)
        bit-rate (p.util/calculate-bit-rate length duration)]
    {:audio/sample-rate sample-rate
     :audio/channels channels
     :audio-video/duration duration
     :audio-video/bit-rate bit-rate
     :musepack/version :sv8}))

(defmethod parse-block-type :replay-gain
  [{bits :bits} _]
  (let [_ (types/read :bytes bits :size 1)
        track-gain (types/read :uint bits :size 2)
        track-peak (types/read :uint bits :size 2)
        album-gain (types/read :uint bits :size 2)
        album-peak (types/read :uint bits :size 2)]
    {:audio-replaygain/track-gain (decode-gain track-gain)
     :audio-replaygain/track-peak (decode-peak track-peak)
     :audio-replaygain/album-gain (decode-gain album-gain)
     :audio-replaygain/album-peak (decode-peak album-peak)}))

(defmethod parse-block-type :encoder-info
  [{bits :bits} _]
  (let [profile (types/read :bits bits :size 4)
        _ (types/read :bits bits :size 28)]
    {:musepack/profile (mpc.util/decode-profile profile)}))

(defmethod parse-block-type :default
  [file header]
  (io/skip-bytes file (:header/size header))
  {})

(defn parse-sv8
  [file]
  (let [blocks (repeatedly #(parse-block file))]
    (->> blocks
         (seq/take-until #(#{:encoder-info :stream-end} (:header/type %)))
         (into {})
         (util/remove-keys-by-ns "header"))))
