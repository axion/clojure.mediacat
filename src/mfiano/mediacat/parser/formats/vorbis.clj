(ns mfiano.mediacat.parser.formats.vorbis
  (:require [mfiano.mediacat.parser.dispatch :as parser]
            [mfiano.mediacat.parser.formats.ogg :as ogg]
            [mfiano.mediacat.parser.io :as io]
            [mfiano.mediacat.parser.types :as types]
            [mfiano.mediacat.parser.util :as p.util]
            [mfiano.mediacat.util :as util]))

(defn- get-total-samples
  [{:keys [path length] :as file}]
  (let [pos (ogg/get-last-page-position file)]
    (io/with-file file path (- length pos)
      (-> file ogg/parse-page :granule-pos))))

(defn- parse-bitrate
  [n]
  (if (pos? n) (/ n 1000.0) false))

(defn- parse-bitrate-mode
  [nominal min max]
  (cond
    (= max nominal min) :constant
    (and nominal (not max) (not min)) :average
    (or max min) :variable
    :else :unspecified))

(defn- parse-common-header
  [{:keys [path bits] :as file}]
  (let [_ (types/read :uint bits :size 1)
        marker (types/read :string bits :size 6)]
    (assert (= marker "vorbis")
            (str "Problem syncing Vorbis identification header while reading file: " path))))

(defn- parse-id-header
  [{bits :bits :as file}]
  (parse-common-header file)
  (let [_ (types/read :bytes bits :size 4)
        channels (types/read :uint bits :size 1)
        sample-rate (types/read :uint bits :size 4 :endian :le)
        bit-rate-max (parse-bitrate (types/read :uint bits :size 4 :endian :le))
        bit-rate-nominal (parse-bitrate (types/read :uint bits :size 4 :endian :le))
        bit-rate-min (parse-bitrate (types/read :uint bits :size 4 :endian :le))
        _ (types/read :bytes bits :size 2)
        samples (get-total-samples file)]
    {:audio/sample-rate sample-rate
     :audio/channels channels
     :audio-video/duration (p.util/calculate-duration samples sample-rate)
     :audio-video/bit-rate-mode (parse-bitrate-mode bit-rate-nominal bit-rate-min bit-rate-max)
     :audio-video/bit-rate bit-rate-nominal}))

(defn parse-vorbis-comments
  [{bits :bits :as file}
   & {ogg? :ogg? :or {ogg? true}}]
  (letfn [(parse-comment []
            (let [len (types/read :uint bits :size 4 :endian :le)
                  [_ & data] (->> (types/read :string bits :size len :encoding :utf8)
                                  (re-find #"([^=]+)=([\s\S]*)"))]
              (vec data)))]
    (when ogg?
      (parse-common-header file))
    (let [vendor-len (types/read :uint bits :size 4 :endian :le)
          _ (types/read :string bits :size vendor-len :encoding :utf8)
          comment-count (types/read :uint bits :size 4 :endian :le)]
      {:common/tags (->> (repeatedly comment-count parse-comment)
                         (group-by first)
                         (reduce-kv (fn [m k v]
                                      (assoc m k (mapv second v)))
                                    {}))})))

(defn- parse-file
  [path]
  (io/with-file file path nil
    (let [file (io/make-lazy-file file ogg/lazy-format-data)]
      (merge {:common/format :vorbis
              :common/file-size (util/calculate-file-size file)
              :common/compression-mode :lossy}
             (parse-id-header file)
             (parse-vorbis-comments file)))))

(defmethod parser/parse-file :ogg
  [path]
  (parse-file path))

(defmethod parser/parse-file :oga
  [path]
  (parse-file path))
