(ns mfiano.mediacat.parser.io
  (:require [clojure.java.io :as io]
            [mfiano.mediacat.parser.util :as p.util])
  (:import (com.tomgibara.bits Bits)))

(defn make-lazy-file
  [file f]
  (->> (f file)
       p.util/make-lazy-bit-reader
       (assoc {} :bits)
       (merge file)))

(defn skip-bytes
  [{stream :stream} count]
  (loop [total 0]
    (when-not (= total count)
      (recur (+ total (.skip stream (- count total)))))))

(defmacro with-file
  [file path eof-offset & body]
  `(let [file# (io/file ~path)]
     (assert (.exists file#)
             (str "File does not exist: " ~path))
     (with-open [stream# (io/input-stream ~path)]
       (let [length# (.length file#)
             ~file {:path ~path
                    :bits (if ~eof-offset
                            (p.util/make-eof-bit-reader ~path length# :offset ~eof-offset)
                            (Bits/readerFrom stream#))
                    :length length#
                    :stream stream#}]
         ~@body))))
