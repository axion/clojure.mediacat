(ns mfiano.mediacat.util
  (:require [clojure.contrib.humanize]
            [clojure.string :as str]))

(defn remove-keys-by-ns
  [s m]
  (reduce-kv
   (fn [m k v]
     (if-not (= (namespace k) s)
       (assoc m k v)
       m))
   {}
   m))

(defn sequence*
  [xf coll]
  (let [rf (xf (fn [xs v] (conj xs v)))
        f (fn f [coll]
            (lazy-seq
             (loop [coll coll]
               (when (seq coll)
                 (if-let [res (rf [] (first coll))]
                   (if (reduced? res)
                     (lazy-cat @res nil)
                     (lazy-cat res (f (rest coll))))
                   (recur (rest coll)))))))]
    (f coll)))

(defn round
  [n precision]
  (let [factor (Math/pow 10 precision)]
    (/ (Math/round (* n factor)) factor)))

(defn seconds->duration
  [n]
  (clojure.contrib.humanize/duration (* n 1000) {:number-format str}))

(defn calculate-file-size
  [{length :length}]
  (round (/ length 1024 1024) 2))
