(ns mfiano.mediacat.parser
  (:require [mfiano.mediacat.parser.dispatch :as dispatch]
            [mfiano.mediacat.parser.formats.flac]
            [mfiano.mediacat.parser.formats.musepack]
            [mfiano.mediacat.parser.formats.vorbis]))

(defn parse-file
  [path]
  (dispatch/parse-file path))
